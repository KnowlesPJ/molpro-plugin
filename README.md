molpro-plugin
=============

This library gives support for programs launched from Molpro.

Documentation is at
[https://molpro.gitlab.io/molpro-plugin]
(https://molpro.gitlab.io/molpro-plugin)
